#include "utils.h"

void printMatrix(int n, double **A)
{
	printf("%2s  ", "  ");
	for (unsigned int j = 0; j < n; ++j)
		printf("%6d ", j);
	printf("\n");
	for (unsigned int i = 0; i < n; ++i)
	{
		printf("%2d [ ", i);
		for (unsigned int j = 0; j < n; ++j)
			printf("%10.3e ", A[i][j]);
		printf("]\n");		
	}

}

void fprintMatrix(FILE *fp, int n, double **A)
{
	for (unsigned int i = 0; i < n; ++i)
	{
		for (unsigned int j = 0; j < n; ++j)
			fprintf(fp, "%16.7e ", A[i][j]);
		fprintf(fp, "\n");		
	}

}

void printVector(int n, double *v)
{
	for (unsigned int i  = 0; i < n; ++i)
		printf("%2d [ %10.3e ]\n", i, v[i]);
}

void fprintVector(FILE *fp, int n, double *v)
{
	for (unsigned int i  = 0; i < n; ++i)
		fprintf(fp, "%16.7e \n", v[i]);
}
