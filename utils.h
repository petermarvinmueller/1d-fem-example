#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>

void printMatrix(int, double **);
void fprintMatrix(FILE*, int, double **);

void printVector(int, double *);
void fprintVector(FILE*, int, double *);

#endif
