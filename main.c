#include <stdlib.h>
#include <stdio.h>
#include "gauss.h"
#include "utils.h"

unsigned int np, ne;
double *x;

const unsigned int npe = 2;
unsigned int *localtoglobal;

const unsigned int nqp = 2;
const double wq[2] = {1.0, 1.0};
const double xq[2] = {-0.577350269189626, 0.577350269189626};
double *J, *invJ, *detJ;

unsigned int ndof;
double **K, *f, *u;


double N(unsigned int i, double xi)
{
	if (i == 0)
		return 0.5 * (1.0 - xi);
	else if (i == 1)
		return 0.5 * (1.0 + xi);
	else 
		return 0.0;
}

double dN(unsigned int i, double xi)
{
	if (i == 0)
		return -0.5;
	else if (i == 1)
		return 0.5;
	else 
		return 0.0;
}

int main(int argc, char **argv)
{
	np = 11;
	ne = np - 1;

	x = (double*)malloc( np * sizeof(double) );
	
	for (unsigned int i = 0; i < np; ++i)
		x[i] = 1.0 / ne * i;

	localtoglobal = (unsigned int*)malloc( npe * ne * sizeof(unsigned int) );
	/*
	 * 	element		i	j
	 *
	 * 	0		0	1
	 * 	1		1	2
	 * 	2		2	3
	 * 	...
	 * 	n-1		n-2	n-1	
	 * 	n		n-1	n
	 *
	 *	[0, 1, 1, 2, 2, 3, 3, 4, 4, 5 ... np-1, np]
	 */
	for (unsigned int e = 0; e < ne; ++e)
		for (unsigned int i = 0; i < npe; ++i)
			localtoglobal[e*npe+i] = e+i;
	
	J = (double*)malloc( ne * nqp * sizeof(double) );
	invJ = (double*)malloc( ne * nqp * sizeof(double) );
	detJ = (double*)malloc( ne * nqp * sizeof(double) );
	for (unsigned int e = 0; e < ne; ++e)
		for (unsigned int p = 0; p < nqp; ++p)
		{
			double l = x[localtoglobal[e*npe+1]] - x[localtoglobal[e*npe+0]];
			J[e*nqp+p] = 0.5 * l;
			invJ[e*nqp+p] = 1.0 / J[e*nqp+p];
			detJ[e*nqp+p] = 0.5 * l;
		}


	/* Setup linear system */
	ndof = np;

	/* Setup stiffnes matrix */
	K = (double**)malloc( ndof * sizeof(double*) );
	for (unsigned int i = 0; i < ndof; ++i)
	{
		K[i] = (double*)malloc( ndof * sizeof(double*) );
		for (unsigned int j = 0; j < ndof; ++j)
			K[i][j] = 0.0;
	}


	for (unsigned int e = 0; e < ne; ++e)
	{
		double Ke[2][2];
		Ke[0][0] = 0.0; Ke[0][1] = 0.0;
		Ke[1][0] = 0.0; Ke[1][1] = 0.0;

		for (unsigned int p = 0; p < nqp; ++p)
			for (unsigned int i = 0; i < npe; ++i)
				for (unsigned int j = 0; j < npe; ++j)
				{
					Ke[i][j] +=  invJ[e*nqp+p] * dN(i,xq[p]) * invJ[e*nqp+p] * dN(j,xq[p]) * detJ[e*nqp+p] * wq[p];	
				}

		for (unsigned int i = 0; i < npe; ++i)
			for (unsigned int j = 0; j < npe; ++j)
				K[localtoglobal[e*npe+i]][localtoglobal[e*npe+j]] += Ke[i][j];

		//printf("Element stiffnes matrix for element %d: \n", e);
		//printMatrix(npe, K);
	}

	printf("Global stiffnes matrix before applying boundary conditions:\n");
	printMatrix(ndof, K);

	/* Setup right hand side */
	f = (double*)malloc( ndof * sizeof(double) );
	
	for (unsigned int e = 0; e < ne; ++e)
	{
		double fe[2];
		fe[0] = 0.0; fe[1] = 0.0;

		for (unsigned int p = 0; p < nqp; ++p)
			for (unsigned int i = 0; i < npe; ++i)
				fe[i] += 1.0 * N(i,xq[p]) * detJ[e*nqp+p] * wq[p];

		//printf("Element right hand side for element %d:\n", e);
		//printVector(npe, fe);

		for (unsigned int i = 0; i < npe; ++i)
			f[localtoglobal[e*npe+i]] += fe[i];
	}

	printf("Global right hand side:\n");
	printVector(ndof, f);

	/* Apply boundary conditions */
	for (unsigned int i = 0; i < ndof; ++i)
	{
		K[i][0] = 0.0;		
		K[0][i] = 0.0;
	}
	K[0][0] = 1.0;
	
	printf("Global stiffnes matrix after applying boundary conditions:\n");
	printMatrix(ndof, K);

	f[0] = 0.0;

	printf("Global right hand side after applying boundary conditions:\n");
	printVector(ndof, f);
	
	u = (double*)malloc( ndof * sizeof(double) );

	for (unsigned int i = 0; i < ndof; ++i) 
		u[i] = f[i];

	printf("Operator:\n");
	printMatrix(ndof, K);

	printf("Right hand side:\n");
	printVector(ndof, u);

	solve(ndof, K, u);

	printf("Solution: \n");
	printVector(ndof, u);

	{
		FILE *fp = fopen("solution.dat", "w");

		fprintf(fp, "%10s %10s\n", "x", "u");		
		for (unsigned int i = 0; i < np; ++i)
			fprintf(fp, "%10.3e %10.3e\n", x[i], u[i]);

		fclose(fp);
	}

	for (unsigned int i = 0; i < ndof; ++i)
		free(K[i]);
	free(K);
	free(f);
	free(u);
	free(detJ);
	free(invJ);
	free(J);
	free(localtoglobal);
	free(x);

	return 0;
}
