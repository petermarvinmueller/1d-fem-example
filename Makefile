run: utils.o gauss.o main.o
	gcc -o $@ $^ -lm

%.o: %.c %.h
	gcc -c -g -O0 -o $@ $<

clean:
	@rm -v run *.o *.dat
