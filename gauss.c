#include "gauss.h"

int solve(unsigned int n, double **K, double *f)
{
	const double eps = 1.e-20;

	double *tmpVec = (double*)malloc( n * sizeof(double) );
	unsigned int *ipiv = (unsigned int*)malloc( n * sizeof(unsigned int) );

	for (unsigned int i = 0; i < n; ++i) 
		ipiv[i] = i;

	for (unsigned int i = 0; i < n-1; ++i)
	{
		// Find largest value in column
		double maxColumnEntry = 0.0;
		unsigned int imax = i;
		for (unsigned int k = i; k < n; ++k)
		{
			double tmp = fabs(K[k][i]);
			if (tmp > maxColumnEntry) 
			{
				maxColumnEntry = tmp;
				imax = k;
			}	
		}

		if (maxColumnEntry < eps) 
		{
			fprintf(stderr, "Error in solve(): Matrix close to singularity");
			free(tmpVec);
			free(ipiv);
			return -1;
		}

		// Pivoting
		if (imax != i)
		{
			// Swap indices
			unsigned int j = ipiv[i];
			ipiv[i] = ipiv[imax];
			ipiv[imax] = j;

			// Swap rows
			for (j = 0; j < n; ++j) tmpVec[j] = K[i][j];
			for (j = 0; j < n; ++j) K[i][j] = K[imax][j];
			for (j = 0; j < n; ++j) K[imax][j] = tmpVec[j];
		}

		for (unsigned int j = i+1; j < n; ++j)
		{
			// Entries of L
			K[j][i] = K[j][i] / K[i][i];

			// Entries of U
			for (unsigned int k = i+1; k < n; ++k)
				K[j][k] -= K[j][i] * K[i][k];
		}
	}

	// f <- Pf
	for (unsigned int j = 0; j < n; ++j) tmpVec[j] = f[j];
	for (unsigned int j = 0; j < n; ++j) f[j] = tmpVec[ipiv[j]];

	// Forward substitution 
	for (unsigned int i = 0; i < n; ++i)
		for (unsigned int k = 0; k < i; ++k)
			f[i] -= K[i][k] * f[k];
	
	// Backward substitution 
	for (unsigned int i = n-1; i > 0; --i)
	{
		for (unsigned int k = i+1; k < n; ++k)
			f[i] -= K[i][k] * f[k];
		
		f[i] /= K[i][i];
	}

	free(ipiv);
	free(tmpVec);
}
